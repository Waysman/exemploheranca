#ifndef SERVIDOR_HPP
#define SERVIDOR_HPP
#include <string>
#include "pessoa.hpp"

using namespace std;

class Servidor : public Pessoa{
private:
	string cargo;
	string departamento;
	int carga_horaria;
public:
	Servidor();
	~Servidor();
	
	string getCargo();
	void setCargo(string cargo);
	string getDepartamento();
	void setDepartamento(string departamento);
	int getCargaHoraria();
	void setCargaHoraria(int carga_horaria);

};



#endif
