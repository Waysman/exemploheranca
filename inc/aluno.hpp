#ifndef ALUNO_HPP
#define ALUNO_HPP
#include <string>
#include "pessoa.hpp"

using namespace std;

class Aluno : public Pessoa{
private:
	string curso;
	float ira;
public:

	Aluno();
	~Aluno();
	string getCurso();
	void setCurso(string curso);
	float getIra();
	void calculaira();

}


#endif
