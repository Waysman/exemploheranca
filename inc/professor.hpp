#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include  <string>
#include "pessoa.hpp"

using namespace std;

class Professor : public Pessoa{
private:
	string formacao;
	string departamento;
	float indice_de_aprovacao;
public:
	Professor();
	~Professor();
	string getFormacao();
	void setFormacao(string formacao);
	string getDepartamento();
	void setDepartamento(string departamento);
	float getIndiceDeAprovacao();
	void setIndiceDeAprovacao(float indice_de_aprovacao);
	

};

#endif
