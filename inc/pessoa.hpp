#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class pessoa{

private:
	int matricula;
	string nome;
	long int cpf;
	string telefone;
	string endereco;
public:
	Pessoa();
	~Pessoa();
	int getMatricula();
	void setMatricula(int matricula);
	string getNome();
	void setNome(string nome);
	long int getCpf();
	void setCpf(long int cpf);
	string getTelefone();
	void setTelefone(string telefone);
	string getEndereco();
	void setEndereco(string endereco);
};


#endif
